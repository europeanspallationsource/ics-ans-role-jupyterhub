ics-ans-role-jupyterhub
=======================

Ansible role to install jupyterhub.

Requirements
------------

- ansible >= 2.2
- molecule >= 1.20

Role Variables
--------------

```yaml
jupyterhub_tag: 0.8.1-ESS2
jupyterhub_docker_container_image: registry.esss.lu.se/ics-docker/notebook
jupyterhub_notebook_dir_volume: /mnt/nfs/home/{username}/files
jupyterhub_unreviewed_example_dir_volume: /opt/jupyterhub/examples/unreviewed
jupyterhub_reviewed_example_dir_volume: /opt/jupyterhub/examples/reviewed
jupyterhub_remove_containers: True
jupyterhub_network: jupyterhub-network
jupyterhub_dir: /opt/jupyterhub
jupyterhub_data: /opt/jupyterhub/data
jupyterhub_admin_users: []
jupyterhub_admin_access: False
jupyterhub_port: 8000
jupyterhub_traefik_host: "{{ansible_fqdn}}"
jupyterhub_traefik_frontend_rule: "Host:{{jupyterhub_traefik_host}}"
jupyterhub_ldap_server_address: 'esss.lu.se'
jupyterhub_ldap_bind_dn_template: []
jupyterhub_ldap_lookup_dn: True
jupyterhub_ldap_search_filter: '(&(samAccountType=805306368)({login_attr}={login}))'
jupyterhub_ldap_user_search_base: 'OU=ESS Users,DC=esss,DC=lu,DC=se'
jupyterhub_ldap_search_user: ldapuser
jupyterhub_ldap_search_password: secret
jupyterhub_ldap_user_attribute: 'sAMAccountName'
jupyterhub_nfs_directories: []
jupyterhub_pull_notebook: False
jupyterhub_dashboard_redirect_url: ""
jupyterhub_dashboard_server_url: ""
jupyterhub_dashboard_auth_token: ""
```

The variable `jupyterhub_nfs_directories` can be used to mount directories via nfs:

```yaml
jupyterhub_nfs_directories:
  - path: /mnt/nfs/home
    src: myserver:/data/home
    opts: timeo=14,intr
```

The variable `jupyterhub_traefik_host` can be set to a single host or a list of hosts:

```yaml
jupyterhub_traefik_host: jupyterhub.domain.com,another.domain.com
```

If you don't want to use a list of Host but a HostRegexp for example, you can override the `jupyterhub_traefik_frontend_rule` variable.

Example Playbook
----------------

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-jupyterhub
```

License
-------

BSD 2-clause

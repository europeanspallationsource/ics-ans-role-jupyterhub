import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    '.molecule/ansible_inventory').get_hosts('all')


def test_jupyterhub_containers(host):
    with host.sudo():
        cmd = host.run('docker ps')
    assert cmd.rc == 0
    # Get the names of the running containers
    # - skip the first line (header)
    # - take the last element of the remaining lines
    names = sorted([line.split()[-1] for line in cmd.stdout.split('\n')[1:]])
    assert names == ['jupyterhub', 'traefik_proxy']


def test_jupyterhub_index(host):
    # This tests that traefik forwards traffic to jupyterhub
    # and that we can access the jupyterhub index page
    cmd = host.run('curl -H Host:jupyterhub.docker.localhost -k -L https://localhost')
    assert '<title>JupyterHub</title>' in cmd.stdout
